#!/bin/bash

set -e

ROBERT2_VERSIONS=(  "0.16.2" "0.17.1" "0.18.1" )
ROBERT2_LATEST_TAG="0.18.1"